#!/bin/bash
#SBATCH --job-name="Bowtie_test"
#SBATCH --mem=4GB
#SBATCH --cpus-per-task=10

#load bowtie

genes=('gfp' 'oct4' 'sox2')

for g in ${genes[@]}; do 
  mkdir -p $g
  printf "#!/bin/bash\n\
#SBATCH --job-name=\"Bowtie_test\"\n\
#SBATCH --mem=4GB\n\
#SBATCH --cpus-per-task=10\n\n\
module load bowtie/1.2.2\n\n\
for file in ~/epigeno/open_analysis/data/$g/* ; do\n\
  path=\"\$(pwd)/\${file##*/}\"
  srun bowtie /shared/projects/2319_ens_hts/data/chipseq/mouse_index/mm9 \$file -p \$SLURM_CPUS_PER_TASK -v 2 -m 1 -3 1 -S 2> \$path.out > \$path.sam\n\
done\n"\
  > "$g/$g.sbatch"
done

