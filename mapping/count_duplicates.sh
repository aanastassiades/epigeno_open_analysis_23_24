#!/bin/bash

module load samtools/1.9

genes=('gfp' 'oct4' 'sox2')

for g in ${genes[@]} ; do 
  cd ~/epigeno/open_analysis/mapping/$g/
  echo $g: 
  srun samtools flagstat marked_merged_${g}.bam
  echo "--------------------------------------------------------------------------------"
done

