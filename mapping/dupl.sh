#!/bin/bash

module load picard/2.22.0

genes=('gfp' 'oct4' 'sox2')

#srun picard MarkDuplicates CREATE_INDEX=true INPUT=SRR576933.bam OUTPUT=Marked_SRR576933.bam METRICS_FILE=metrics VALIDATION_STRINGENCY=STRICT

for g in ${genes[@]} ; do 
  input="INPUT=merged_$g.bam"
  output="OUTPUT=marked_merged_$g.bam"
  options="CREATE_INDEX=true METRICS_FILE=metrics VALIDATION_STRINGENCY=STRICT"
  cd ~/epigeno/open_analysis/mapping/$g/
  echo $g: 
  srun picard MarkDuplicates $options $input $output
done

