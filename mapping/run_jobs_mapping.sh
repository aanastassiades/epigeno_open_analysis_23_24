#!/bin/bash

genes=('gfp' 'oct4' 'sox2')

for g in ${genes[@]} ; do 
  cd ~/epigeno/open_analysis/mapping/$g
  pwd
  sbatch $g.sbatch
done
