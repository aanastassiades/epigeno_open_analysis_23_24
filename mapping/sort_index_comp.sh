#!/bin/bash

module load samtools/1.9

#genes=('gfp' 'oct4' 'sox2')
genes=('oct4' 'sox2')

for g in ${genes[@]} ; do
  cd ~/epigeno/open_analysis/mapping/$g
  for file in ./*.sam ; do 
   filename="${file%.*}"
   echo "file $filename"
   srun samtools sort $file | samtools view -b > "$filename.bam"
   echo "index"
   srun samtools index "$filename.bam"
   echo "compress"
   gzip $file
  done
done
