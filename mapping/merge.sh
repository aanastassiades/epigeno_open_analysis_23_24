#!/bin/bash

module load picard/2.22.0

genes=('gfp' 'oct4' 'sox2')

#picard MergeSamFiles ASSUME_SORTED=false I=SRR001996.fastq.bam I=SRR001997.fastq.bam I=SRR001998.fastq.bam I=SRR001998.fastq.bam O=merged_gfp_false_sorted.bam

for g in ${genes[@]} ; do 
  input=""
  output="O=merged_$g.bam"
  sorted="ASSUME_SORTED=true"
  for file in ~/epigeno/open_analysis/mapping/$g/SRR*.bam ; do 
    input="$input I=${file##*/}"
  done
  cd ~/epigeno/open_analysis/mapping/$g/
  echo $g: 
  srun picard MergeSamFiles $sorted$input $output
done

