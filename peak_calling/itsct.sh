#!/bin/bash

module load bedtools/2.30.0

genes=('oct4' 'sox2')
conds=('2' '5' '10')

for c in ${conds[@]}; do
  bedtools intersect -a ${genes[0]}_${c}/${genes[0]}_merged_peaks.bed -b ${genes[1]}_${c}/${genes[1]}_merged_peaks.bed > intersect_${c}.bed
done

