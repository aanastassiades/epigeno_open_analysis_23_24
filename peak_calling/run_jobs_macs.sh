#!/bin/bash

genes=('oct4' 'sox2')
conds=('2' '5' '10')

for g in ${genes[@]}; do 
for c in ${conds[@]}; do
  sbatch ${g}_${c}/job.sbatch
done
done

