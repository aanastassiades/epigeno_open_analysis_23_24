#!/bin/bash

genes=('oct4' 'sox2')
conds=('2' '5' '10')

for g in ${genes[@]}; do 
for c in ${conds[@]}; do
  mkdir -p "${g}_${c}"
  printf "#!/bin/bash\n\
#SBATCH --job-name=\"macs$g-$c\"\n\
#SBATCH --mem=4GB\n\
#SBATCH --cpus-per-task=10\n\n\
module load macs/1.4.3\n\n

target=\"-t /shared/home/aanastassiades/epigeno/open_analysis/mapping/$g/merged_$g.bam\"
control=\"-c /shared/home/aanastassiades/epigeno/open_analysis/mapping/gfp/merged_gfp.bam\"
name=\"--name ${g}_merged\"
bandwidth=\"--bw 500\"                                                                      # 500 because size in the article (?)
fold=\"-m $c,30\"
gsize=\"--gsize mm\"                                                   
params=\"--format BAM --diag\"
cd ${g}_${c}
srun macs \$target \$control \$name \$bandwidth \$gsize \$fold \$params &> MACS_oct4.out"\
  > "${g}_${c}/job.sbatch"
done
done

