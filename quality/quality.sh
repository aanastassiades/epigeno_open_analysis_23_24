#!/bin/bash

module load fastqc/0.11.8

#genes=('gfp' 'oct4' 'sox2')
genes=('sox2')

for g in ${genes[@]}; do
  mkdir -p $g
  for file in ~/epigeno/open_analysis/data/$g/*; do
    echo $file
    srun fastqc $file -o ./$g
    echo 
  done
done
