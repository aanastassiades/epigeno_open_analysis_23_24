#!/bin/bash

genes=('gfp' 'oct4' 'sox2')

for g in ${genes[@]}; do
  firefox ${g}/*.html
done
