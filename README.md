# Open Analysis
## Scripts
Dans l'ordre de ma pipeline
- `download.sh`: télécharger les datasets
- `unzip.sh` : dézipper les datasets
- `quality.sh` : executer fastqc sur tous les datasets
- `open.sh` : visualiser la qualité avec firefox
- `generate_jobs_mapping.sh` : générer fichier batch pour chaque TF
- `run_jobs_mapping.sh` : executer les batch
- `sort_index_comp.sh` : transformer SAM en BAM, indexing et compression des SAM *pourrait batch pour aller plus vite* (quelques sbatch fait main)
- `merge.sh` : fusionner les BAM pour chaque TF
- `dupl.sh` : marquer les duplicats
- `count_duplicates` : compter les duplicats avec `samtools`
- peak_calling : comme pour le mapping (`generate` et `run`) MACS de plusieurs conditions (Voir partie **Paramètres**)
- `itsct.sh` : bedtools intersect entre sox2 et oct4 pour chaque conditions testées sur MACS

# Paramètres
- Taille de la fenêtre dans pour *peak calling* : 500bp
- Taille effective du génome (d'après MACS 'mm') : 1.87e9
- MFOLD de *x* a 30 (pourquoi changer le upper?) avec *x*:
    - 10 (défaut)
    - 2
    - 5

# Analyse de motifs
- générer les fichiers fasta sur [Galaxy](https://usegalaxy.fr/)
- utiliser [RSAT](http://rsat.sb-roscoff.fr/) pour trouvers les motifs

Pour le moment seul l'analyse des paramètres par défaut ont été faits (sox2 et oct4) dans le dossier `motifs`.

# Todo
- [x] Déterminer quels sont les bons contrôles 
- [x] Emmener les fichiers sur le serveurs
- [x] Visualiser
- [x] Changer le MFOLD pour le *peak calling* (selon l'article) **Faire un script meta pour batcher**
