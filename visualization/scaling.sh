#!/bin/bash


genes=('gfp' 'oct4' 'sox2')

#srun bamCoverage --bam ~/cours_chipseq/02-Mapping/IP/Marked_SRR576933.bam --outFileName SRR576933_nodup.bw --outFileFormat bigwig --effectiveGenomeSize 4639675 --normalizeUsing RPGC --skipNonCoveredRegions --extendReads 200 --ignoreDuplicates

for g in ${genes[@]} ; do 
  input="--bam /shared/home/aanastassiades/epigeno/open_analysis/mapping/$g/marked_merged_$g.bam"
  output="--outFileName nodup_$g.bw"
  outfmt="--outFileFormat bigwig"
  size="--effectiveGenomeSize 1870000000"                                  # according to MACS
  extreads="--extendReads 500"                                               # 500 because size in the article (?)
  params="--normalizeUsing RPGC --skipNonCoveredRegions --ignoreDuplicates"
  echo $g: 
  srun bamCoverage $input $output $outfmt $size $extreads $params
done

