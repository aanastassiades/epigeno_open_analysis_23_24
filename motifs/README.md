# But de ce dossier 
Ce dossier sert à stocker les résultats de la recherche de motifs.
Par soucis de taille je n'incluerai pas ces fichiers dans le dépôt.

# Obtention des fichiers
Après utilisation de l'outil `getFasta` sur le site Galaxy, j'ai utilisé les paramètres détaillés dans [le tutoriel](https://github.com/morganeTC/M2courseENS2022/blob/main/hands-on/motif_disco.md) pour l'analyse de motif.
Par la suite, j'ai utilisé la comparaison à la base de données Jaspar afin d'oberver des ressemblances à des endroits du génome.(?)
