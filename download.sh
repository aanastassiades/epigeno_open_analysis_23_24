#!/bin/bash

oct4=(
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002013/SRR002013.fastq.gz"
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002014/SRR002014.fastq.gz"
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002015/SRR002015.fastq.gz"
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002012/SRR002012.fastq.gz"
  )

sox2=(
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002026/SRR002026.fastq.gz"
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002025/SRR002025.fastq.gz"
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002023/SRR002023.fastq.gz"
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR002/SRR002024/SRR002024.fastq.gz"
  )

gfp=(
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR001/SRR001997/SRR001997.fastq.gz" 
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR001/SRR001998/SRR001998.fastq.gz" 
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR001/SRR001996/SRR001996.fastq.gz" 
  "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR001/SRR001999/SRR001999.fastq.gz"
  )


if [[ $1 = "oct4" ]]; then
  ls=${oct4[@]}
fi

if [[ $1 = "sox2" ]]; then
  ls=${sox2[@]}
fi

if [[ $1 = "gfp" ]]; then
  ls=${gfp[@]}
fi

mkdir -p "data/$1"
cd "data/$1"

for l in ${ls[@]}; do
  srun wget $l 
done

